import express from "express";
import { weatherCall } from "../controllers/weather.js";
const router = express.Router();

router.get("/", weatherCall)
router.get("/hi", (req, res) => {
    res.send("Hello World");
});


export default router;