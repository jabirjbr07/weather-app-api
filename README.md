# Weather app api

This is an api for weather forecast application built with Node.js and Express on the .This application works as backend for the Weather forecasting app. The API fetches weather data from the OpenWeatherMap API, which provides current weather data and forecast for up to 5 days and next 5 hours. The api is hosted on AWS EC2.



## File structures:-
### -Index.js (main file)
### -routes folder which maintainn api routes
### -controller folder which contain weather-controller file api logics and functions
### -utils folder which contain multiple files for different utility functions
        -error.js :- handles errors overall the api
        -localTimeConverter.js :- converts timestamp into actual date and time of each timezone using luxon 
        -formatweatherdata.js:- formats the weather data from OpenWeatherMap api and return needed data only
        -formatforcastData.js:- formats the weatherforecast data from OpenWeatherMap api and returns next five hours forecast and       next five days forecast data.



## API Endpoints:-
GET /api/weather?{queries}
(the queries includes city name / city lan and lon, unit type etc...)

